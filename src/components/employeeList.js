import React, { Component } from 'react'
import '../index.scss';
import './common/common.scss';
import './common/table.scss';
import { userService } from '../services/userService.js';
import { Table } from 'react-bootstrap'; 
import Grid from '@material-ui/core/Grid';
import massagePic from "../images/massage.png";

class EmployeeList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            error: null,
            users: []
        };
    }


    componentDidMount() {
        userService.getByRole().then(users => this.setState({ users }));
    }

    render() {
        const { users } = this.state;
        return (
            <Grid container spacing={3}>
                <Grid item xs={1}>
                </Grid>
                <Grid item xs={7}>
                    <h1>Employees</h1>
                    <Table className="table-no-border table-center table-full-width">
                        <thead className="btn-primary">

                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.map(users => (
                                <tr key={users.id}>
                                    <td>
                                        <img
                                            src={users.picture}
                                            width="100"
                                            height="100"
                                            alt="profilepic"
                                        />
                                    </td>

                                    <td> <b>{users.fullname} </b>
                                        <br></br> {users.email}
                                        <br></br> {users.skills}
                                        <br></br> {users.infotext}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Grid>

            <Grid item xs={4}>
                    <br></br>
                    <img
                        src={massagePic}
                        style={{ width: '100%' }}
                        alt="massage"
                    />
            </Grid>
        </Grid>
    );
    }
}
export default EmployeeList; 
