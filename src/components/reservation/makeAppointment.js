
import React from 'react';
import { history } from '../../helpers/history.js';
import { userService } from '../../services/userService.js';
import { serviceService } from '../../services/serviceService.js';
import '../../index.scss';
import '../common/common.scss';
class MakeAppointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            users: [],
            service: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        userService.getByRole().then(users => this.setState({ users }));
        serviceService.getAll().then(service => this.setState({ service }));
    }
    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }
    handleSubmit(event) {
        alert('Your Reservation: ' + ' serviceselect= ' + this.state.serviceselect + ' and employeeselect= ' + this.state.employeeselect);
        event.preventDefault();
    }
    render() {
        const { users, service } = this.state;
        return (
            <div history={history} className="container">
                <form onSubmit={this.handleSubmit} className="empLabel">
                    <div>
                        <label>
                            Select employee:<br></br>
                            <select key={users}
                                value={this.state.users.id}
                                name="employeeselect"
                                onChange={this.handleChange}>
                                <option>----- Select Employee -----</option>
                                {users.map(users => (
                                    <option value={users.fullname}>{users.fullname}</option>
                                ))}
                            </select>
                        </label>
                    </div>
                    <div>
                        <label>
                            Select service:<br></br>
                            <select key={service}
                                value={this.state.service.ServiceId}
                                name="serviceselect"
                                onChange={this.handleChange}>
                                <option>------- Select Service -------</option>
                                {service.map(service => (
                                    <option value={service.ServiceId}>{service.name}</option>
                                ))}
                            </select>
                        </label>
                    </div>
                    <br></br>
                    <div>
                        <button onClick={this.handleSubmit}
                            className="submitReservation"> Go to calendar
                            </button>
                    </div>
                </form>
            </div>
        );
    }
}
export default MakeAppointment; 