import React from 'react';
import { Button } from 'react-bootstrap';
import { userService } from '../../services/userService.js';
import { history } from '../../helpers/history.js';
import '../../index.scss';
import '../common/common.scss';


class UserList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            fullname: '',
            role: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleEditName = this.handleEditName.bind(this);
        this.handleEditRole = this.handleEditRole.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        userService.getAll().then(users => this.setState({ users }));
    }

    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }

    handleEditName(user) {
        userService.editUserList(
            user.id,
            this.state.fullname,
            user.email,
            user.password,
            user.picture,
            user.phonenumber,
            user.skills,
            user.infotext,
            user.role);
        window.location.reload(true);
    }

    handleEditRole(user) {
        userService.editUserList(
            user.id,
            user.fullname,
            user.email,
            user.password,
            user.picture,
            user.phonenumber,
            user.skills,
            user.infotext,
            this.state.role);
        window.location.reload(true);
    }

    handleDelete(user) {
        if (window.confirm('Are You sure?')) {
            userService.deleteUser(user);
        }
        window.location.reload(true);
    }

    render() {
        return (
            <div history={history}
                className="container">
                <div>
                    <div>
                        <h2>User List</h2>
                        <table className="table-no-border table-center table-full-width">
                            <thead>
                                <tr>
                                    <th>#ID</th>
                                    <th>Full Name</th>
                                    <th>Email</th>

                                    <th>Role</th>
                                </tr>
                            </thead>

                            <tbody>
                                {this.state.users.map(user => (
                                    <tr key={user.id}>
                                        <td>{user.id}</td>
                                        <td>
                                            <label htmlFor="fullname">
                                                <input
                                                    placeholder={user.fullname}
                                                    type="text"
                                                    name="fullname"
                                                    onChange={this.handleChange}
                                                />
                                            </label>

                                            <Button
                                                className="buttonGreen"
                                                type="button"
                                                onClick={() => this.handleEditName(user)}
                                                variant="info">Edit
                                            </Button>
                                        </td>

                                        <td>{user.email}</td>

                                        <td>
                                            <label htmlFor="role">
                                                <input
                                                    placeholder={user.role}
                                                    type="text"
                                                    name="role"
                                                    onChange={this.handleChange}
                                                />
                                            </label>

                                            <Button
                                                className="buttonGreen"
                                                type="button"
                                                onClick={() => this.handleEditRole(user)}
                                                variant="info">Edit
                                            </Button>
                                        </td>

                                        <td>
                                            <Button
                                                className="buttonRed"
                                                type="button"
                                                onClick={() => this.handleDelete(user.id)}
                                                variant="danger">Delete Profile
                                            </Button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserList; 
