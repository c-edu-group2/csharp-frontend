import React from 'react';
import { authenticationService } from '../../services/authenticationService.js';
import { userService } from '../../services/userService.js';
import { Button } from 'react-bootstrap';
import Grid from '@material-ui/core/Grid';
import '../../index.scss';
import '../common/common.scss';
import "../form.scss";


class EditEmployeeInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue,
            picture: '',
            phonenumber: '',
            skills: '',
            infotext: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleEditPic = this.handleEditPic.bind(this);
        this.handleEditPhone = this.handleEditPhone.bind(this);
        this.handleEditSkills = this.handleEditSkills.bind(this);
        this.handleEditInfotext = this.handleEditInfotext.bind(this);
    }

    componentDidMount() {
        const { currentUser } = this.state;
        userService.getById(currentUser.id).then(userFromApi =>
            this.setState({ userFromApi }));
    }

    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }

    handleEditPic(currentUser) {
        userService.editEmpInfo(
            currentUser.id,
            currentUser.fullname,
            currentUser.email,
            this.state.picture,
            currentUser.phonenumber,
            currentUser.skills,
            currentUser.infotext,
            currentUser.role);
        window.location.reload(true);
    }

    handleEditPhone(currentUser) {
        userService.editEmpInfo(
            currentUser.id,
            currentUser.fullname,
            currentUser.email,
            currentUser.picture,
            this.state.phonenumber,
            currentUser.skills,
            currentUser.infotext,
            currentUser.role);
        window.location.reload(true);
    }

    handleEditSkills(currentUser) {
        userService.editEmpInfo(
            currentUser.id,
            currentUser.fullname,
            currentUser.email,
            currentUser.picture,
            currentUser.phonenumber,
            this.state.skills,
            currentUser.infotext,
            currentUser.role);
        window.location.reload(true);
    }

    handleEditInfotext(currentUser) {
        userService.editEmpInfo(
            currentUser.id,
            currentUser.fullname,
            currentUser.email,
            currentUser.picture,
            currentUser.phonenumber,
            currentUser.skills,
            this.state.infotext,
            currentUser.role);
        window.location.reload(true);
    }

    render() {
        const { currentUser } = this.state;
        return (
            <Grid container spacing={2}
                onSubmit={this.handleSubmit}>
                <Grid item xs={4}>
                    <div className="empLabel">

                        <label htmlFor="picture"> Picture </label>
                        <input
                            placeholder={currentUser.picture}
                            type="text"
                            name="picture"
                            onChange={this.handleChange}
                        />
                    </div>

                    <Button
                        className="empUpdateButton"
                        type="button"
                        onClick={() => this.handleEditPic(currentUser)}
                        variant="info">Update Picture
                        </Button>
                </Grid>

                <Grid item xs={8}>
                    <div className="empLabel">
                        <label htmlFor="skills"> Skills </label>
                        <input
                            placeholder={currentUser.skills}
                            type="text" name="skills"
                            onChange={this.handleChange}
                        />
                    </div>

                    <Button
                        className="empUpdateButton"
                        type="button"
                        onClick={() => this.handleEditSkills(currentUser)}
                        variant="info">Update Skills
                     </Button>
                </Grid>

                <Grid item xs={4}>
                    <div className="empLabel">
                        <label htmlFor="phonenumber"> Phonenumber </label>
                        <input
                            placeholder={currentUser.phonenumber}
                            type="text"
                            name="phonenumber"
                            onChange={this.handleChange}
                        />
                    </div>

                    <Button
                        className="empUpdateButton"
                        type="button"
                        onClick={() => this.handleEditPhone(currentUser)}
                        variant="info">Update Phonenumber
                    </Button>
                </Grid>

                <Grid item xs={8}>
                    <div className="empLabel">
                        <label htmlFor="infotext"> Infotext </label>
                        <input
                            placeholder={currentUser.infotext}
                            type="text"
                            name="infotext"
                            onChange={this.handleChange}
                        />
                    </div>

                    <Button
                        className="empUpdateButton"
                        type="button"
                        onClick={() => this.handleEditInfotext(currentUser)}
                        variant="info">Update Infotext
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

export default EditEmployeeInfo;
