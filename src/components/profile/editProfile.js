import React from 'react';
import { authenticationService } from '../../services/authenticationService.js';
import { userService } from '../../services/userService.js';
import '../../index.scss';
import '../common/common.scss';
import "../form.scss";
import Grid from '@material-ui/core/Grid';
import FormPasswordReset from './formPasswordReset.js';


class EditProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue
        };
    }

    componentDidMount() {
        const { currentUser } = this.state;
        userService.getById(currentUser.id).then(userFromApi =>
            this.setState({ userFromApi }));
    }

    render() {
        const { currentUser } = this.state;
        return (
            <Grid container spacing={8}>
                <Grid item xs={1}>
                </Grid>

                <Grid item xs={4}>
                    <h1>Hi {currentUser.fullname}!</h1>
                    <p>Welcome to your profile page!
                    You can make changes to your password,
                    if you want to change name or email-address, pleace contact on admin
                    to delete your profile and make new.</p>
                </Grid>

                <Grid item xs={1}>
                </Grid>

                <Grid item xs={4}>
                    <form>
                        <div className="fullname">
                            <label htmlFor="fullname"> Name </label>
                            <input
                                placeholder={currentUser.fullname}
                                type="fullname"
                                name="fullname"
                                readOnly
                            />
                        </div>

                        <div className="email">
                            <label htmlFor="email"> Email </label>
                            <input
                                placeholder={currentUser.email}
                                type="email"
                                name="email"
                                readOnly
                            />
                        </div>
                    </form>
                    <FormPasswordReset />
                </Grid>

                <Grid item xs={1}>
                </Grid>
            </Grid>
        );
    }
}

export default EditProfile;