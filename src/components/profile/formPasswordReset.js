import React, { Component } from 'react'
import { Formik } from 'formik'
import { object, ref, string } from 'yup'
import FormHelperText from '@material-ui/core/FormHelperText'
import Spinner from './spinner.js'
import { authenticationService } from '../../services/authenticationService.js';
import { userService } from '../../services/userService.js';
import '../../index.scss'
import '../common/common.scss'
import '../form.scss'

export default class FormPasswordReset extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: authenticationService.currentUserValue
        };
    }

    componentDidMount() {
        const { currentUser } = this.state;
        userService.getById(currentUser.id).then(userFromApi =>
            this.setState({ userFromApi }));
    }

    _handleSubmit = ({
        newPass,
    }) => {
        const { currentUser } = this.state;
        userService.editUser(
            currentUser.id,
            currentUser.fullname,
            currentUser.email,
            newPass,
            currentUser.picture,
            currentUser.phonenumber,
            currentUser.skills,
            currentUser.infotext,
            currentUser.role);
        window.location.reload(+true);
    }

    render() {
        return (
            <Formik
                initialValues={{
                    newPass: '',
                    confirmPass: '',
                }}
                validationSchema={object().shape({
                    newPass: string()
                        .min(6, 'Password has to be longer than 6 characters!')
                        .required('New password is required'),
                    confirmPass: string()
                        .oneOf([ref('newPass')], 'Passwords do not match')
                        .required('Password is required'),
                })}
                onSubmit={(
                    { newPass, confirmPass },
                    { setSubmitting, resetForm }
                ) =>
                    this._handleSubmit({
                        newPass,
                        confirmPass,
                        setSubmitting,
                        resetForm,
                    })
                }
                render={props => {
                    const {
                        values,
                        touched,
                        errors,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isValid,
                        isSubmitting,
                    } = props
                    return isSubmitting ? (
                        <Spinner />
                    ) : (
                            <div>
                                <form onSubmit={handleSubmit}>
                                    <div className="password" >
                                        <label htmlFor="password-new"
                                            >New Password </label>
                                        <input
                                            id="password-new"
                                            name="newPass"
                                            type="password"
                                            value={values.newPass}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            
                                        />
                                        <FormHelperText
                                            error={Boolean(touched.newPass && errors.newPass)}
                                        >
                                            {touched.newPass && errors.newPass ? errors.newPass : ''}
                                        </FormHelperText>
                                    </div>

                                    <div className="password">
                                        <label htmlFor="password-confirm"
                                            >Confirm Password </label>
                                        <input
                                            id="password-confirm"
                                            name="confirmPass"
                                            type="password"
                                            value={values.confirmPass}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            
                                        />
                                        <FormHelperText
                                            error={Boolean(touched.confirmPass && errors.confirmPass)}
                                        >
                                            {touched.confirmPass && errors.confirmPass
                                                ? errors.confirmPass
                                                : ''}
                                        </FormHelperText>
                                    </div>

                                    <button className="buttonOrange"
                                        type="submit"
                                        variant="raised"
                                        color="primary"
                                        disabled={Boolean(!isValid || isSubmitting)}
                                    >
                                        {'Reset Password'}
                                    </button>
                                </form>
                            </div>
                        )
                }}
            />
        )
    }
}
