import React, { Component } from 'react'
import { history } from '../../helpers/history.js';
import { userService } from '../../services/userService.js';
import '../../index.scss';
import '../common/common.scss';

class AddEmployeeToList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullname: '',
            email: '',
            password: '',
            picture: '',
            phonenumber: '',
            skills:'',
            infotext: '',
            role: 'Employee'
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }

    handleSubmit(event) {
        event.preventDefault();
        userService.addEmployee(
            this.state.fullname,
            this.state.email,
            this.state.password,
            this.state.picture,
            this.state.phonenumber,
            this.state.skills,
            this.state.infotext,
            this.state.role);

        window.location.reload(true);
    }

    render() {
        return (
            <div history={history}
                onSubmit={this.handleSubmit}
                className="container center">

                <h1> Add Employee </h1>

                <table className="table-no-border table-center table-full-width">
                    <thead>
                        <tr>
                            <th>Fullname</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Phonenumber</th>                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="fullname">
                                    <input
                                        type="text"
                                        name="fullname"
                                        onChange={this.handleChange} />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="email">
                                    <input
                                        type="text"
                                        name="email"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="password">
                                    <input
                                        type="password"
                                        name="password"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="phonenumber">
                                    <input
                                        type="text"
                                        name="phonenumber"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>
                        </tr>
                    </tbody>

                    <thead>
                        <tr>
                            <th>Picture</th>
                            <th>Skills</th>
                            <th>Infotext</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="picture">
                                    <input
                                        type="text"
                                        name="picture"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="skills">
                                    <input
                                        type="text"
                                        name="skills"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="infotext">
                                    <input
                                        type="text"
                                        name="infotext"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <button
                                    onClick={this.handleSubmit}
                                    className="buttonAddEmployee"> Add Employee
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default AddEmployeeToList; 