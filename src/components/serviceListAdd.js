import React, { Component } from 'react'
import { history } from '../helpers/history.js';
import { serviceService } from '../services/serviceService.js';
import '../index.scss';
import './common/common.scss';


class ServiceListAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            duration: '',
            price: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }

    handleSubmit(event) {
        event.preventDefault();
        serviceService.addService(
            this.state.name,
            this.state.duration,
            this.state.price);
        window.location.reload(true);
    }

    render() {
        return (
            <div history={history}
                onSubmit={this.handleSubmit}
                className="container center">

                <h1> Service list Add</h1>
                <table className="table-no-border table-center table-full-width">
                    <thead>
                        <tr>
                            <th>Service</th>
                            <th>Duration min</th>
                            <th>Price {'\u20AC'}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label htmlFor="name">
                                    <input
                                        type="text"
                                        name="name" 
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="duration">
                                    <input
                                        type="number"
                                        name="duration"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <label htmlFor="price">
                                    <input
                                        type="number"
                                        name="price"
                                        onChange={this.handleChange}
                                    />
                                </label>
                            </td>

                            <td>
                                <button
                                    onClick={this.handleSubmit}
                                    className="buttonOrange">Add Service
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ServiceListAdd; 