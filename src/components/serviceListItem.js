import React, { Component } from 'react'
import '../index.scss';
import './common/common.scss';
import { Table } from 'react-bootstrap';
import Grid from '@material-ui/core/Grid';
import { serviceService } from '../services/serviceService.js';
import massagePic from "../images/massage.png";

class ServiceListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            service: []
        }
    }

    componentDidMount() {
        serviceService.getAll().then(service => this.setState({ service }));
    }

    render() {
        const { error, service } = this.state;

        if (error) {
            return (
                <div>Error: {error.message}</div>
            )
        } else {
            return (
                <Grid container spacing={3}>
                    <Grid item xs={1}>
                    </Grid>
                    <Grid item xs={7}>
                        <h1>Service</h1>
                        <Table className="table-no-border table-center table-full-width">
                            <thead className="btn-primary">

                                <tr>
                                    <th>Service</th>
                                    <th>Duration min</th>
                                    <th>Price {'\u20AC'}</th>
                                    <th> </th>
                                </tr>
                            </thead>

                            <tbody>
                                {service.map(service => (
                                    <tr key={service.id}>

                                        <td>{service.name}</td>
                                        <td>{service.duration}  </td>
                                        <td>{service.price}</td>
                                        <td> </td>
                                    </tr>
                                ))}
                            </tbody>

                        </Table>
                    </Grid>

                    <Grid item xs={4}>
                        <br></br><img
                            src={massagePic}
                            style={{ width: '100%' }}
                            alt="massage"
                        />
                    </Grid>

                </Grid>
            );
        }
    }
}

export default ServiceListItem; 