import React from 'react';
import '../../../src/index.scss';
import './common.scss';
import './footer.scss';

function Footer() {

    return (
        <div className="footer">
            <p>customerservice(at)physio.com<br></br>
               +35844 123 4567<br></br>
               Teknologiantie 16<br></br>
               Oulu
            </p>
        </div>
    );
}

export default Footer;