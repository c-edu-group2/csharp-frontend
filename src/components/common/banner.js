import React from 'react';
import '../../../src/index.scss';
import './banner.scss';
import './common.scss';
import bannerPic from "../../images/banner_pic.jpg";
import logoPic from "../../images/physio_logo.jpg";

console.log("LOG: Load banner"); 

function Banner() {

    return (
        <div className="container" >
            <img src={bannerPic} style={{ width: '100%' }} alt="banner" />
            <div>
                <div className="topleft">
                    <img src={logoPic} style={{ height: '15vh' }} alt="logo" />
                </div>           
            </div>
        </div>
    );
}

export default Banner;