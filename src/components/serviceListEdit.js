import React from 'react'
import { Button } from 'react-bootstrap';
import { serviceService } from '../services/serviceService.js';
import { history } from '../helpers/history.js';
import '../index.scss';
import './common/common.scss';


class ServiceListEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            service: [],
            name: '',
            duration: '',
            price: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleEditName = this.handleEditName.bind(this);
        this.handleEditDuration = this.handleEditDuration.bind(this);
        this.handleEditPrice = this.handleEditPrice.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        serviceService.getAll().then(service => this.setState({ service }));
    }

    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value }, () => console.log(this.state));
    }

    handleEditName(service) {
        serviceService.editServiceList(
            service.id,
            this.state.name,
            service.duration,
            service.price);
        window.location.reload(true);
    }

    handleEditDuration(service) {
        serviceService.editServiceList(
            service.id,
            service.name,
            this.state.duration,
            service.price);
        window.location.reload(true);
    }

    handleEditPrice(service) {
        serviceService.editServiceList(
            service.id,
            service.name,
            service.duration,
            this.state.price);
        window.location.reload(true);
    }

    handleDelete(service) {
        if (window.confirm('Are You sure?')) {
            serviceService.deleteService(service);
        }
        window.location.reload(true);
    }

    render() {
        const { error, service } = this.state;

        if (error) {
            return (
                <div>Error: {error.message}</div>
            )
        } else {
            return (
                <div history={history}
                    className="container">

                    <h1> Service list Edit/Delete</h1>

                    <table className="table-no-border table-center table-full-width">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Service</th>
                                <th>Duration min</th>
                                <th>Price {'\u20AC'}</th>
                            </tr>
                        </thead>

                        <tbody>
                            {service.map(service => (
                                <tr key={service.id}>
                                    <td>{service.id}</td>
                                    <td>
                                        <label htmlFor="name">
                                            <input
                                                placeholder={service.name}
                                                type="text"
                                                name="name"
                                                onChange={this.handleChange}
                                            />
                                        </label>
                                        <Button
                                            className="buttonGreen"
                                            type="button"
                                            onClick={() => this.handleEditName(service)}
                                            variant="info">Edit
                                        </Button>
                                    </td>

                                    <td>
                                        <label htmlFor="duration">
                                            <input
                                                placeholder={service.duration}
                                                type="text"
                                                name="duration"
                                                onChange={this.handleChange}
                                            />
                                        </label>
                                        <Button
                                            className="buttonGreen"
                                            type="button"
                                            onClick={() => this.handleEditDuration(service)}
                                            variant="info">Edit
                                        </Button>
                                    </td>

                                    <td>
                                        <label htmlFor="price">
                                            <input
                                                placeholder={service.price}
                                                type="text" name="price"
                                                onChange={this.handleChange}
                                            />
                                        </label>
                                        <Button
                                            className="buttonGreen"
                                            type="button"
                                            onClick={() => this.handleEditPrice(service)}
                                            variant="info">Edit
                                        </Button>
                                    </td>

                                    <td>
                                        <Button
                                            className="buttonRed"
                                            type="button"
                                            onClick={() => this.handleDelete(service.id)}
                                            variant="danger">Delete Service
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            )
        }
    }
}

export default ServiceListEdit; 
