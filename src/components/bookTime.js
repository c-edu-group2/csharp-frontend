import React, { Component } from 'react'
import Paper from '@material-ui/core/Paper';
import { ViewState,  IntegratedEditing, EditingState } from '@devexpress/dx-react-scheduler';
import {
    Scheduler,
    DayView,
    WeekView,
    MonthView,
    Appointments,
    ViewSwitcher,
    Toolbar,
    AppointmentTooltip,
    AppointmentForm,
    TodayButton,
    DateNavigator,
    DragDropProvider,

} from '@devexpress/dx-react-scheduler-material-ui';

import { appointmentService } from '../services/appointmentService.js';

class BookTime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            appointment: [],
            currentViewName: 'work-week',
            currentDay: new Date(),
            employees: [],
            title: '',
            startDate: '',
            endDate: '',
        };
        this.commitChanges = this.commitChanges.bind(this);
        this.currentDateChange = (currentDate) => { this.setState({ currentDate }); };
        this.currentViewNameChange = (currentViewName) => {
            this.setState({ currentViewName });

        };

    }

    componentDidMount() {
        appointmentService.getAll().then(appointments => {
            const newAppointments = appointments.map((appointment) => {
                return {
                    ...appointment,

                }
            })
            this.setState({ appointment: newAppointments });
        });
    }

    commitChanges({ added, changed, deleted  }) {
        this.setState((state) => {
            let appointments = state.appointment;
            if (added) {
                const startingAddedId = appointments.length > 0 ? appointments[appointments.length - 1].id + 1 : 0;
                appointments = [...appointments, { id: startingAddedId, ...added }];
                const appointment = appointments[appointments.length - 1];
                appointmentService.addAppointment(appointment).then(appointment => {

                    return {
                        
                        ...appointment,

                    }
                });
                window.location.reload(true);
            }
            if (changed) {
                    appointments = appointments.map(appointments => (
                        changed[appointments.id] ? { ...appointments, ...changed[appointments.id] } : appointments));
            }
            
            if (deleted !== undefined) {
                const appointmentToDelete = appointments.find(appointment => appointment.id === deleted);
                appointments = appointments.filter(appointment => appointment.id !== deleted);
                appointmentService.deleteAppointment(appointmentToDelete.id).then(appointment => {
                    return {
                        ...appointment,
                        id: appointment.id,
                    }
                })
                window.location.reload(true);
            }

            return { appointments };
        });
    }


    render() {
        const { appointment, currentViewName, currentDate, } = this.state;

        return (
            <div>
                <Paper>
                    <Scheduler
                        height={660}
                        data={appointment}
                    >
                        <ViewState
                            defaultcurrentDate={currentDate}
                            onCurrentDateChange={this.currentDateChange}
                            currentViewName={currentViewName}
                            onCurrentViewNameChange={this.currentViewNameChange}
                        />
                        <EditingState
                            onCommitChanges={this.commitChanges}
                        />
                        <DayView />
                        <WeekView
                            name="work-week"
                            displayName="Work Week"
                            excludedDays={[0, 6]}
                            startDayHour={9}
                            endDayHour={17}
                        />
                        <WeekView
                            startDayHour={9}
                            endDayHour={17}
                        />
                        <MonthView />
                        <Toolbar />
                        <ViewSwitcher />
                        <DateNavigator />
                        <TodayButton />
                        <Appointments />
                        <IntegratedEditing />
                        <AppointmentTooltip
                            showCloseButton
                            showDeleteButton
                            showOpenButton />
                        <AppointmentForm/>
                        <DragDropProvider />
                    </Scheduler>
                </Paper>
            </div>
        );
    }
}


export default BookTime;
