import React from 'react';
import { history } from '../helpers/history.js';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import "./form.scss";

import { userService } from '../services/userService.js';
import { authenticationService } from '../services/authenticationService.js';


class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        if (authenticationService.currentUserValue) {
            this.props.history.push('/');
        }

    }
    render() {
        return (
            <div>
                <Formik history={history}
                    initialValues={{
                        fullname: '',
                        email: '',
                        password: ''
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                            .email('E-mail is not valid!')
                            .required('E-mail is required!'),
                        password: Yup.string()
                            .min(6, 'Password has to be longer than 6 characters!')
                            .required('Password is required!')
                    })}
                    onSubmit={({ fullname, email, password }, { setStatus, setSubmitting }) => {
                        setStatus();
                        userService.register(fullname, email, password)
                            .then(user => {
                                console.log(user);
                                history.push('/login');
                            }
                            )
                            .catch(error => {
                                setSubmitting(false);
                                setStatus(error);
                            });
                    }}
                    render={({ errors, status, touched, isSubmitting }) => (
                        <div className="container center">
                            <div className="wrapper">
                                <Form className="form-wrapper">
                                    <h1> Create account </h1>

                                    <div className="fullname">
                                        <label htmlFor="fullname"> Name </label>
                                        <Field
                                            name="fullname"
                                            type="text"
                                            className={'form-control' + (errors.fullname && touched.fullname ? ' is-invalid' : '')}
                                        />
                                        <ErrorMessage
                                            name="fullname"
                                            component="div"
                                            className="invalid-feedback"
                                        />
                                    </div>

                                    <div className="email">
                                        <label htmlFor="email"> Email </label>
                                        <Field
                                            name="email"
                                            type="text"
                                            className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')}
                                        />
                                        <ErrorMessage
                                            name="email"
                                            component="div"
                                            className="invalid-feedback"
                                        />
                                    </div>

                                    <div className="password">
                                        <label htmlFor="password"> Password </label>
                                        <Field
                                            name="password"
                                            type="password"
                                            className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')}
                                        />
                                        <ErrorMessage
                                            name="password"
                                            component="div"
                                            className="invalid-feedback"
                                        />
                                    </div>

                                    <div className="form-group login">
                                        <button type="submit"
                                            className="btn btn-primary"
                                            disabled={isSubmitting}>Register
                                            </button>
                                    </div>

                                    {status &&
                                        <div className={'alert alert-danger'}>{status}</div>
                                    }
                                    <Link to={'/login'} className="FormField__Link">I'm already member</Link>
                                </Form>
                            </div>
                        </div>
                    )}
                />
            </div>
        )
    }
}


export default RegisterForm;
