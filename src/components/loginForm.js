import React from 'react';
import { history } from '../helpers/history.js';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import "./form.scss";
import { authenticationService } from '../services/authenticationService.js';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        // redirect to home if already logged in
        if (authenticationService.currentUserValue) {
            this.props.history.push('/');
        }
    }

    render() {
        return (
            <div>
                <Formik history={history}
                    initialValues={{
                        email: '',
                        password: ''
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string().required('Email is required'),
                        password: Yup.string().required('Password is required')
                    })}
                    onSubmit={({ email, password }, { setStatus, setSubmitting }) => {
                        setStatus();
                        authenticationService.login(email, password)
                            .then(user => {
                                console.log(user);
                                history.push('/');
                                }
                            )
                            .catch(error => {
                                setSubmitting(false);
                                setStatus(error);
                            });
                    }}
                    render={({ errors, status, touched, isSubmitting }) => (
                        <div className="container center">
                            <div className="wrapper">
                                <Form className="form-wrapper">
                                    <h1> Login </h1>

                                    <div className="form-group email">
                                        <label
                                            htmlFor="email">Email
                                        </label>
                                        <Field
                                            name="email"
                                            type="text"
                                            className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')}
                                        />
                                        <ErrorMessage
                                            name="email"
                                            component="div"
                                            className="invalid-feedback"
                                        />
                                    </div>

                                    <div className="form-group password">
                                        <label
                                            htmlFor="password">Password
                                        </label>
                                        <Field
                                            name="password"
                                            type="password"
                                            className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')}
                                        />
                                        <ErrorMessage
                                            name="password"
                                            component="div"
                                            className="invalid-feedback"
                                        />
                                    </div>

                                    <div className="form-group login">
                                        <button
                                            type="submit"
                                            className="btn btn-primary"
                                            disabled={isSubmitting}>Login
                                        </button>
                                    </div>

                                    {status &&
                                        <div
                                            className={'alert alert-danger'}>{status}
                                        </div>
                                    }

                                    <Link to={'/register'}
                                        className="FormField__Link">Not member yet
                                    </Link>
                                </Form>
                            </div>
                        </div>
                    )}
                />
            </div>
        )
    }
}

export { LoginForm };