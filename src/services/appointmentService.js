
import { authHeader } from '../helpers/auth-header.js';
import { handleResponse } from '../helpers/handle-response.js';
import lbDate from "lbdate";
import moment from "moment";

lbDate.init(moment);

const replacer = lbDate.getReplacer();
const apiUrl = 'http://localhost:53785';


export const appointmentService = {
    getAll,
    addAppointment,
    deleteAppointment,

};

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: {
                'Content-Type': 'application/json'
            },
    };
    return fetch(`${apiUrl}/appointment/`, requestOptions)
        .then(handleResponse)
        .then(appointment => {
            console.log('Success:', appointment);
            return appointment;
        });
}


function addAppointment(appointment) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(appointment, replacer)
    };
    return fetch(`${apiUrl}/appointment`, requestOptions)
        .then(handleResponse)
        .then((appointment) => {
            console.log('Success:', appointment);
            alert('appointment added')
            return (appointment);
        });
}

function deleteAppointment(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/appointment/${id}`, requestOptions)
        .then(handleResponse)
        .then(appointment => {
            console.log('Success:', appointment);
            alert('appointment deleted')
            return appointment;
        });
}