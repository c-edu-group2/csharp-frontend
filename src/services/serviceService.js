
import { authHeader } from '../helpers/auth-header.js';
import { handleResponse } from '../helpers/handle-response.js';
import { authenticationService } from '../services/authenticationService.js';

const currentUser = authenticationService.currentUserValue;
const apiUrl = 'http://localhost:53785';

export const serviceService = {
    getAll,
    addService,
    deleteService,
    editServiceList
};

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/service`, requestOptions).then(handleResponse);
}

function addService(name, duration, price) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.token}`
        },
        body: JSON.stringify({ name, duration, price })
    };
    return fetch(`${apiUrl}/service`, requestOptions)
        .then(handleResponse)
        .then(service => {
            console.log('Success:', service);
            alert('Service added')
            return service;
        });
}

function editServiceList(id, name, duration, price) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.token}`
        },
        body: JSON.stringify({ name, duration, price })
    };

    return fetch(`${apiUrl}/service/${id}`, requestOptions)
        .then(handleResponse)
        .then(editServiceList => {
            console.log('Success:', editServiceList);
            alert('Service edited')
            return editServiceList;
        })
        .catch((error) => {
            console.error('Error:', error);
            alert('Something happened')
        });
}

function deleteService(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/service/${id}`, requestOptions).then(handleResponse);
}