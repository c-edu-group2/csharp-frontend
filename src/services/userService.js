import { authHeader } from '../helpers/auth-header.js';
import { handleResponse } from '../helpers/handle-response.js';
import { authenticationService } from '../services/authenticationService.js';

const currentUser = authenticationService.currentUserValue;
const apiUrl = 'http://localhost:53785';

export const userService = {
    register,
    editUser,
    getAll,
    getById,
    getByRole,
    getByRoleId,    
    deleteUser,
    editUserList,
    addEmployee,
    editEmpInfo
};

function register(fullname, email, password) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ fullname, email, password })
    };

    return fetch(`${apiUrl}/users/register`, requestOptions)
        .then(handleResponse)
        .then(user => {
            console.log('Success:', user);
            alert('Register success, now pleace log in')
            return user;
        });
}

function editUser(id, fullname, email, password, picture, phonenumber, skills, infotext, role) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.token}`
        },
        body: JSON.stringify({ fullname, email, password, picture, phonenumber, skills, infotext, role })
    };
    return fetch(`${apiUrl}/users/${id}`, requestOptions)
        .then(handleResponse)
        .then(editUser => {
            console.log('Success:', editUser);
            alert('Success')
            return editUser;
        })
        .catch((error) => {
            console.error('Error:', error);
            alert('Something happened')
        });
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/users`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function getByRole() {
    const requestOptions = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify()
    };
    return fetch(`${apiUrl}/users/employee/Employee`, requestOptions).then(handleResponse);
}

function getByRoleId(id) {
   
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/users/employee/Employee/${id}`, requestOptions).then(handleResponse);
}

function deleteUser(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };
    return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function editUserList(id, fullname, email, password, picture, phonenumber, skills, infotext, role) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.token}`
        },
        body: JSON.stringify({ fullname, email, password, picture, phonenumber, skills, infotext, role })
    };
    return fetch(`${apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function addEmployee(fullname, email, password, picture, phonenumber, skills, infotext, role) {
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ fullname, email, password, picture, phonenumber, skills, infotext, role })
    };
    return fetch(`${apiUrl}/users/addNewEmployee`, requestOptions)
        .then(handleResponse)
        .then(user => {
            console.log('Success:', user);
            alert('User added')
            return user;
        })
        .catch((error) => {
            console.error('Error:', error);
            alert('Error occurred')
        });
}

function editEmpInfo(id, fullname, email,password, picture, phonenumber, skills, infotext, role) {
    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${currentUser.token}`
        },
        body: JSON.stringify({ fullname, email, password, picture, phonenumber, skills, infotext, role })
    };
    return fetch(`${apiUrl}/users/${id}`, requestOptions)
        .then(handleResponse)
        .then(user => {
            console.log('Success:', user);
            alert('Success')
            return user;
        })
        .catch((error) => {
            console.error('Error:', error);
            alert('Something happened')
        });
}

