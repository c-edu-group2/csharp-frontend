import React from 'react';
import { Router, Switch, Route, Link } from 'react-router-dom';
import Banner from "./components/common/banner.js";
import Home from "./pages/home.js";
import Employee from "./pages/employee.js";
import Servicelist from "./pages/servicelist.js";
import ServicelistAdmin from "./pages/servicelistAdmin.js";
import Reservation from "./pages/reservation.js";
import Login from "./pages/login.js";
import Register from "./pages/register.js";
import Footer from "./components/common/footer.js";
import AdminProfile from "./pages/adminProfile.js";
import EmployeeProfile from "./pages/employeeProfile.js";
import UserProfile from "./pages/userProfile.js";
import { history } from './helpers/history.js';
import { role } from './helpers/role.js';
import { authenticationService } from './services/authenticationService.js';
import { PrivateRoute } from './components/privateRoute.js';
import './App.scss';
import './components/common/common.scss';
import './components/common/navbar.scss';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null,
            isAdmin: false,
            isEmployee: false,
            isUser: false
        };
    }

    componentDidMount() {
        authenticationService.currentUser.subscribe(x => this.setState({
            currentUser: x,
            isAdmin: x && x.role === role.Admin,
            isEmployee: x && x.role === role.Employee,
            isUser: x && x.role === role.User
        }));
    }

    logout() {
        authenticationService.logout();
        history.push('/login');
    }

    render() {
        const { currentUser, isAdmin, isEmployee, isUser } = this.state;
        return (
            <div className="container">
                <Banner />
                <Router history={history}>
                    <nav>
                        <div className="topnav">
                            <div>
                                <Link to={'/'}> Home </Link>
                                <Link to={'/employee'}>Employee</Link>
                                <Link to={'/servicelist'}>Service</Link>
                                {(currentUser && isAdmin) &&
                                    <Link to={'/servicelistAdmin'}>Servicelist</Link>
                                }
                                {(currentUser && isEmployee) &&
                                    <Link to={'/reservation'}>Reservation</Link>
                                }
                                {(currentUser && isUser) &&
                                    <Link to={'/reservation'}>Reservation</Link>
                                }
                            </div>
                            <div className="topnav-right">
                                {currentUser &&
                                    <Link
                                        to={'/'}
                                        onClick={this.logout}>Logout
                                    </Link>
                                }
                                {!currentUser &&
                                    <Link
                                        to={'/login'}>Login
                                    </Link>
                                }
                                {!currentUser &&
                                    <Link
                                        to={'/register'}>Register
                                    </Link>
                                }
                                {(currentUser && isAdmin) &&
                                    <Link
                                        to={'/adminProfile'}>Profile
                                    </Link>
                                }
                                {(currentUser && isEmployee) &&
                                    <Link
                                        to={'/employeeProfile'}>Profile
                                    </Link>
                                }
                                {(currentUser && isUser) &&
                                    <Link
                                        to={'/userProfile'}>Profile
                                    </Link>
                                }
                            </div>
                        </div>
                    </nav>
                    <div className="center-text">
                        <Switch>
                            <Route
                                exact path='/'
                                component={Home}
                            />
                            <Route
                                path='/employee'
                                component={Employee}
                            />
                            <Route
                                path='/servicelist'
                                roles={[role.User][role.Employee]}
                                component={Servicelist}
                            />
                            <Route
                                path='/servicelistAdmin'
                                roles={[role.Admin]}
                                component={ServicelistAdmin}
                            />
                            <Route
                                path='/login'
                                component={Login}
                            />
                            <Route
                                path='/register'
                                component={Register}
                            />

                            <PrivateRoute
                                path='/adminProfile'
                                roles={[role.Admin]}
                                component={AdminProfile}
                            />
                            <PrivateRoute
                                path='/employeeProfile'
                                roles={[role.Employee]}
                                component={EmployeeProfile}
                            />
                            <PrivateRoute
                                path='/userProfile'
                                roles={[role.User]}
                                component={UserProfile}
                            />
                            <PrivateRoute
                                path='/reservation'
                                roles={[role.User][role.Employee]}
                                component={Reservation}
                            />

                        </Switch>
                    </div>
                </Router>

                <Footer />
            </div>
        );
    }
}

export { App }; 
