import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import '../index.scss';
import '../components/common/common.scss';
import massagePic from "../images/massage.png";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    }
}));

function Home() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={1}>
                </Grid>
                <Grid item xs={7}>
                    <h1>Welcome to Physio service!</h1>
                    <p> 
                        Have you injured a muscle recently? Are you unable to get rid of your backache?
                        Do you have swollen joints? Are you trying to rehabilitate from an automobile accident?
                        Do you suffer from osteoarthritis? Are you sick of bearing pain for years?
                        Do you want to find the clear answer to these problems?
                        Who can help you best, a physiotherapist or a massage therapist?
                    </p>

                    <p>
                        <b>Physiotherapy</b> services treat acute and chronic injuries and help with injury prevention.
                        Rehabilitation is the main focus of physiotherapy.
                        Physiotherapy is a compulsory component of managing many critical diseases.
                    </p>

                    <p>
                        <b>Massage</b> therapy is used for muscle relaxation and muscular pain alleviation.
                        Massage therapy provides relief for muscular pain.
                        Massage therapy can provide mental relaxation and stress relief in a calm setting through relaxation massage and central nervous system stimulation.
                        Massage therapy is tailored for overall wellness.
                    </p>

                    <p>
                        Please,
                        <b> login/register </b>
                        to servive and You can do suitable appointment.
                    </p>
                </Grid>
                <Grid item xs={4}>

                    <br></br>
                    <img
                        src={massagePic}
                        style={{ width: '100%' }}
                        alt="massage"
                    />
                </Grid>
            </Grid>
        </div>
    );    
}

export default Home;