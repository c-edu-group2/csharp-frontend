import React from 'react';
import  ServiceListItem  from '../components/serviceListItem.js';
import '../index.scss';
import '../components/common/common.scss';
import '../components/common/table.scss';

function Servicelist() {

    return (
        <div className="container center text-color text-center">
            <br></br>
            <ServiceListItem />
        </div>
    );
}

export default Servicelist;