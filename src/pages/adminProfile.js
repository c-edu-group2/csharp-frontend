import { React } from 'react';
import EditProfile from "../components/profile/editProfile.js";
import AddEmployeeToList from "../components/profile/addEmployee.js";
import UserList from "../components/profile/userList.js";
import '../index.scss';
import '../components/common/common.scss';

function adminProfile() {
    return (
        <div className="container center">
            <div>
                <EditProfile />
                <hr></hr>
            </div>

            <div>
                <AddEmployeeToList />
                <hr></hr>
            </div>

            <div>
                <UserList />
                <hr></hr>
            </div>
        </div>
    );
}
export default adminProfile;