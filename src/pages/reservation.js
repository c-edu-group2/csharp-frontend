import React from 'react';
import BookTime from '../components/bookTime.js';
import '../index.scss';
import '../components/common/common.scss';
import '../components/common/table.scss';


function Reservation() {

    return (
        <div className="container center text-color text-center">
            <br></br>
            <BookTime />
        </div>
    );
}

export default Reservation;

