import React from 'react';
import '../index.scss';
import '../components/common/common.scss';
import EmployeeList from '../components/employeeList.js';
import '../components/common/table.scss';


function Employee() {

    return (
        <div className="container center text-color text-center">
            <div>
                <EmployeeList />
                <hr></hr>
            </div>
        </div>
    );
}

export default Employee;