import React from 'react';
import '../index.scss';
import '../components/common/common.scss';
import RegisterForm from '../components/registerForm.js';

function Register() {

    return (
        <div className="container">
            <div className="text-color text-center">
                <br />
                <RegisterForm />
            </div>
        </div>
    );
}

export default Register;
