import React from 'react';
import '../index.scss';
import '../components/common/common.scss';
import { LoginForm } from '../components/loginForm.js';

function LoginContent() {

    return (
        <div className="container">
            <div className="text-color text-center">
                <br />
                <LoginForm />
            </div>
        </div>
    );
}

export default LoginContent;
