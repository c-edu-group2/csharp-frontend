import { React } from 'react';
import EditProfile from "../components/profile/editProfile.js";

import '../index.scss';
import '../components/common/common.scss';

function userProfile() {
    return (
        <div className="container center">
            <div>
                <EditProfile />
                <hr></hr>
                
            </div>
        </div>
    );
}
export default userProfile;