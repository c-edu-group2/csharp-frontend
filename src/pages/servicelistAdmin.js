import React from 'react';
import ServiceListAdd from '../components/serviceListAdd.js';
import ServiceListEdit from '../components/serviceListEdit.js';
import '../index.scss';
import '../components/common/common.scss';
import '../components/common/table.scss';

function ServicelistAdmin() {

    return (
        <div className="container center text-color text-center">
            <br></br>
            <ServiceListAdd />
            <hr></hr>
            <ServiceListEdit />
        </div>
    );
}

export default ServicelistAdmin;