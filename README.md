# CSharp edu group2 / CSharp frontend

CSharp frontend library is a React library for dealing with Physiotherapy time reservation.
The project has been done as a CSharp course exercise work in a small group.

Project includes:
- User register/login
- Authentication
- User roles for admin, employee and user (customer)
- Admin is able to add/edit/delete services, edit/delete users and add new employees
- Calendar for booking physiotherapy service
- Profile password change for all Roles
- Validation for password and email

Technologies used:
- React
- material-ui
- react-scheduler-material-ui
- Cors is used to connect frontend to backend

## Installation

GitLab repository: https://gitlab.com/c-edu-group2/csharp-frontend.git

Use the node.js to install CSharp frontend library React environment:
- npm install
- npm start

## Usage

Also CSharp backend git repository and pgAdmin4 database are needed for CSharp frontend library usage.

## Known bugs and missing parts

Unfortunately time ran out at our course and some needed parts are missing from our application:
- retrieve a specific employee's calendar for the logged-in user and the user's own bookings
- calendar for a specific service
- authentication missing from calendar
- reservation end time was mentioned to be calculated from start time and service duration

Sometimes getting token errors from authentication when trying some user operations at the first time after logged in app.